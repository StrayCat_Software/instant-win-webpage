Dear Sir or Madam,

I have included here the source code for the Instant-Win project.

This is available to view online at www.pbikes.site50.net.

Unfortunately, due to using a free web hosting site, adverts can be disruptive:
Please understand that these can, on rare occasions, prevent access to the dialogue
for allowing location services to be used, which will prevent the JavaScript application
from working correctly. One of these sites in particular includes a message saying that 
you have won a prize, and so is easy to misconstrue for the actual site.
If this happens, please clear the cache and refresh the page.

The project follows the brief, and has an added feature: The user must be at the
location of the shop for it to work at all. If they are not, a message telling
them to go to the shop will be displayed in place of whether or not they have won.
The location has been set as Proxama's Norwich office, but the radius around it should
the whole city.

I have also included a feature which prevents users from simply refreshing the page
to have another attempt at winning: The date of the user's last visit is saved as
a string to JavaScript local storage: if the page is refreshed, local storage is 
checked for today's date, and if it is found, a message telling the user that they
have already had a turn for that day is displayed in place of the win or lose message.

With the use of local storage to prevent cheating by refreshing in mind, it is worth noting
that in order to test this web-application, it is important to clear the browser's cache
before refreshing, which will clear the date from local storage, allowing for another turn
to take place. It is understood that this in itself is something that a user could take
advantage of, but short of storing results in a database, it was seen as the only way of
preventing users taking advantage in this way, whilst still allowing them to play the next
day, as it simply makes it more difficult to cheat.

If you do choose to test the source code provided here, please be advised that for it to work
you will need to use some kind of local server. This is because of the use of 
browser-based location services, which are denied by default for JavaScript files
which do not originate from a server.

Kind regards,

Daniel Pawsey.