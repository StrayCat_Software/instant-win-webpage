var shopLatitude = 0.0;
var shopLongitude = 0.0;
var radius = 0.0;
var prefix = '<div class="row"><div class="col-md-4">';
var suffix = '</div></div>';
var congratsString = prefix + '<h2>You\'re a winner!</h2><p id="CongratsParagraph">Congratulations! Show your phone to a member of staff to collect your prize.</p>' + suffix;
var unluckyString = prefix + '<h2>In-Store Competition</h2><p id="unluckyParagraph">Unlucky! You haven\'t won this time, try again tomorrow.</p>' + suffix;
var wrongLocationString = prefix + '<h2>In-Store Competition</h2><p id="wrongLocationParagraph">See us in store for a chance to win!</p>' + suffix;
var tommorowString = prefix + '<h2>In-Store Competition</h2><p id="tomorrowParagraph">You\'ve already played today. Check back tomorrow for another chance to win!</p>' + suffix;

function init() {
    //alert("init() called");
    getShopLocationDetails();
    getLocation();
}

/**
 * 
 * @returns {undefined}
 */
function getShopLocationDetails() {
    //alert("getShopLocationDetails() called");

    $(document).ready(function () {
        $.getJSON("json/ShopLocation.json", function (result) {
            shopLatitude += parseFloat(result.latitude);
            shopLongitude += parseFloat(result.longitude);
            radius += parseFloat(result.radius);
        });
    });
}

/**
 * Get the user's location
 * 
 * @returns {undefined}
 */
function getLocation() {
    //alert("getLocation() called");
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
                function (position) {
                    checkLocationIsCorrect(position);
                },
                function (error) {
                    //alert(error.message);
                    $('#competitionResults').empty();
                    $('#competitionResults').append(wrongLocationString);
                }, {
            enableHighAccuracy: true
            , timeout: 5000
        }
        );
    }

    /** 
     * Check whether the user is in the shop
     * 
     * @param {type} position The device's current location.
     * @returns {undefined}
     */
    function checkLocationIsCorrect(position) {
        //alert("checkLocationIsCorrect(position) called");
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;

        if ((latitude > (shopLatitude - radius) && latitude < (shopLatitude + radius)) && (longitude > (shopLongitude - radius) && longitude < (shopLongitude + radius))) {
            //alert("within radius");
            assignRandomNumber();
        }
        else {
            //alert("not within radius");
            $('#competitionResults').empty();
            $('#competitionResults').append(wrongLocationString);
        }
    }

    /**
     * Assign a random number to the user if they have not already viewed the page
     * today.
     * 
     * @returns {undefined}
     */
    function assignRandomNumber() {
        var date = new Date();
        var dateString = date.toDateString();

        // Check browser support
        if (typeof (Storage) != "undefined") {

            // Check whether the user has checked this page today. If not, proceed.
            if (!localStorage.getItem("assignedDate")) {

                if (localStorage.getItem("assignedDate") !== dateString) {

                    // Put today's date into local storage.
                    localStorage.setItem("assignedDate", dateString);

                    // Assign random number
                    var assignedNumber = Math.floor((Math.random() * 5));

                    // Check whether the assigned number is the winning number
                    if (assignedNumber === 0) {
                        $('#competitionResults').empty();
                        $('#competitionResults').append(congratsString);
                    }
                    else {
                        $('#competitionResults').empty();
                        $('#competitionResults').append(unluckyString);
                    }
                }
                else {
                    $('#competitionResults').empty();
                    $('#competitionResults').append(unluckyString);
                }
            }
            else {
                $('#competitionResults').empty();
                $('#competitionResults').append(tommorowString);
            }
        }
    }
}